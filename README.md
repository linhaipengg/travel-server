
- 项目名称：景点旅游项目服务端（travel-server）
- 技术架构：SpringCloud + Mybatis + MySQL + Redis + RabbitMQ + Sentinel + Docker + Nginx
- 项目描述：一个基于微服务的个性化旅游景点推荐系统，通过个性化推荐的旅游景点，为广大用户提供全方位、高效率的旅游服务。项目基础架构采用JDK8、SpringBoot、SpringCloud Alibab构建，完成每日签到、景点查询、点赞收藏评论、景点排行榜以及门票下单等业务。底层采用Redis缓存、RabbitMQ消息队列、Redisson分布式锁以及分库分表等技术，支持海量用户购买门票。

