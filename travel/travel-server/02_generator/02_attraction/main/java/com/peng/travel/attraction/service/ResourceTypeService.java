package com.peng.travel.attraction.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.peng.common.utils.PageUtils;
import com.peng.travel.attraction.entity.ResourceTypeEntity;

import java.util.Map;

/**
 * 
 *
 * @author peng
 * @email haipeng_lin@163.com
 * @date 2024-03-24 09:46:23
 */
public interface ResourceTypeService extends IService<ResourceTypeEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

