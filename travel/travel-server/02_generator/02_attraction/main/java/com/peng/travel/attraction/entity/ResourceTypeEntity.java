package com.peng.travel.attraction.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author peng
 * @email haipeng_lin@163.com
 * @date 2024-03-24 09:46:23
 */
@Data
@TableName("ams_resource_type")
public class ResourceTypeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 资源类型id，自增
	 */
	@TableId
	private Integer resourceTypeId;
	/**
	 * 资源类型【自然类、人文类、复合类、主题公园、社会类】
	 */
	private String resourceType;
	/**
	 * 资源类型介绍
	 */
	private String introduction;
	/**
	 * 添加时间
	 */
	private LocalDateTime addTime;
	/**
	 * 修改时间
	 */
	private LocalDateTime updateTime;

}
