package com.peng.travel.attraction.dao;

import com.peng.travel.attraction.entity.AttractionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author peng
 * @email haipeng_lin@163.com
 * @date 2024-03-24 09:46:23
 */
@Mapper
public interface AttractionDao extends BaseMapper<AttractionEntity> {
	
}
