package com.peng.travel.attraction.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.peng.common.utils.PageUtils;
import com.peng.common.utils.Query;

import com.peng.travel.attraction.dao.AttractionDao;
import com.peng.travel.attraction.entity.AttractionEntity;
import com.peng.travel.attraction.service.AttractionService;


@Service("attractionService")
public class AttractionServiceImpl extends ServiceImpl<AttractionDao, AttractionEntity> implements AttractionService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AttractionEntity> page = this.page(
                new Query<AttractionEntity>().getPage(params),
                new QueryWrapper<AttractionEntity>()
        );

        return new PageUtils(page);
    }

}