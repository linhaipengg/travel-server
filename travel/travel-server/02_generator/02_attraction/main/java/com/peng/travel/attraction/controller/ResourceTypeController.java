package com.peng.travel.attraction.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.peng.travel.attraction.entity.ResourceTypeEntity;
import com.peng.travel.attraction.service.ResourceTypeService;
import com.peng.common.utils.PageUtils;
import com.peng.common.utils.R;



/**
 * 
 *
 * @author peng
 * @email haipeng_lin@163.com
 * @date 2024-03-24 09:46:23
 */
@Slf4j
@RestController
@RequestMapping("attraction/resourcetype")
public class ResourceTypeController {
    @Autowired
    private ResourceTypeService resourceTypeService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissionss("attraction:resourcetype:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = resourceTypeService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{resourceTypeId}")
    //@RequiresPermissionss("attraction:resourcetype:info")
    public R info(@PathVariable("resourceTypeId") Integer resourceTypeId){
		ResourceTypeEntity resourceType = resourceTypeService.getById(resourceTypeId);

        return R.ok().put("resourceType", resourceType);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissionss("attraction:resourcetype:save")
    public R save(@RequestBody ResourceTypeEntity resourceType){
		resourceTypeService.save(resourceType);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissionss("attraction:resourcetype:update")
    public R update(@RequestBody ResourceTypeEntity resourceType){
		resourceTypeService.updateById(resourceType);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissionss("attraction:resourcetype:delete")
    public R delete(@RequestBody Integer[] resourceTypeIds){
		resourceTypeService.removeByIds(Arrays.asList(resourceTypeIds));

        return R.ok();
    }

}
