package com.peng.travel.booking.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.peng.common.utils.PageUtils;
import com.peng.common.utils.Query;

import com.peng.travel.booking.dao.TicketDao;
import com.peng.travel.booking.entity.TicketEntity;
import com.peng.travel.booking.service.TicketService;


@Service("ticketService")
public class TicketServiceImpl extends ServiceImpl<TicketDao, TicketEntity> implements TicketService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<TicketEntity> page = this.page(
                new Query<TicketEntity>().getPage(params),
                new QueryWrapper<TicketEntity>()
        );

        return new PageUtils(page);
    }

}