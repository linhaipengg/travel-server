package com.peng.travel.booking.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author peng
 * @email haipeng_lin@163.com
 * @date 2024-03-24 10:01:48
 */
@Data
@TableName("bms_ticket")
public class TicketEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 门票id，自增
	 */
	@TableId
	private Integer ticketId;
	/**
	 * 景点id
	 */
	private Integer attractionId;
	/**
	 * 门票名称
	 */
	private String name;
	/**
	 * 门票类型【0为成人票、1为学生票、2为儿童票】
	 */
	private Integer typeId;
	/**
	 * 门票介绍
	 */
	private String introduction;
	/**
	 * 价格
	 */
	private Double price;
	/**
	 * 上架状态【1为上架，0为下架】
	 */
	private Integer status;
	/**
	 * 门票数量
	 */
	private Integer num;
	/**
	 * 添加时间
	 */
	private LocalDateTime addTime;
	/**
	 * 修改时间
	 */
	private LocalDateTime updateTime;

}
