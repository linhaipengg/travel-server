package com.peng.travel.booking.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author peng
 * @email haipeng_lin@163.com
 * @date 2024-03-24 10:01:48
 */
@Data
@TableName("bms_order")
public class OrderEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 订单id，全局id
	 */
	@TableId
	private Integer orderId;
	/**
	 * 用户id
	 */
	private Integer userId;
	/**
	 * 景点id
	 */
	private Integer attractionId;
	/**
	 * 门票id
	 */
	private Integer ticketId;
	/**
	 * 购买数量
	 */
	private Integer num;
	/**
	 * 订单价格
	 */
	private Double price;
	/**
	 * 订单状态【0为待支付，1为已支付，2为已使用，3为已取消】
	 */
	private Integer status;
	/**
	 * 添加时间
	 */
	private LocalDateTime addTime;
	/**
	 * 修改时间
	 */
	private LocalDateTime updateTime;

}
