package com.peng.travel.booking.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author peng
 * @email haipeng_lin@163.com
 * @date 2024-03-24 10:01:48
 */
@Data
@TableName("bms_comment")
public class CommentEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 评论id
	 */
	@TableId
	private Integer commentId;
	/**
	 * 订单记录id
	 */
	private Integer orderId;
	/**
	 * 用户id
	 */
	private Integer userId;
	/**
	 * 景点id
	 */
	private Integer attractionId;
	/**
	 * 评分（0-10）
	 */
	private Integer score;
	/**
	 * 评分内容
	 */
	private String content;
	/**
	 * 评分图片
	 */
	private String images;
	/**
	 * 添加时间
	 */
	private LocalDateTime addTime;
	/**
	 * 修改时间
	 */
	private LocalDateTime updateTime;

}
