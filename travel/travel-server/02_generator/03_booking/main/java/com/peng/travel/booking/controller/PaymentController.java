package com.peng.travel.booking.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.peng.travel.booking.entity.PaymentEntity;
import com.peng.travel.booking.service.PaymentService;
import com.peng.common.utils.PageUtils;
import com.peng.common.utils.R;



/**
 * 
 *
 * @author peng
 * @email haipeng_lin@163.com
 * @date 2024-03-24 10:01:48
 */
@Slf4j
@RestController
@RequestMapping("booking/payment")
public class PaymentController {
    @Autowired
    private PaymentService paymentService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissionss("booking:payment:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = paymentService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{paymentId}")
    //@RequiresPermissionss("booking:payment:info")
    public R info(@PathVariable("paymentId") Integer paymentId){
		PaymentEntity payment = paymentService.getById(paymentId);

        return R.ok().put("payment", payment);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissionss("booking:payment:save")
    public R save(@RequestBody PaymentEntity payment){
		paymentService.save(payment);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissionss("booking:payment:update")
    public R update(@RequestBody PaymentEntity payment){
		paymentService.updateById(payment);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissionss("booking:payment:delete")
    public R delete(@RequestBody Integer[] paymentIds){
		paymentService.removeByIds(Arrays.asList(paymentIds));

        return R.ok();
    }

}
