package com.peng.travel.user.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author peng
 * @email haipeng_lin@163.com
 * @date 2024-03-24 09:25:21
 */
@Data
@TableName("ums_user")
public class UserEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 用户id，自增
	 */
	@TableId
	private Integer userId;
	/**
	 * 用户名，唯一
	 */
	private String username;
	/**
	 * 手机号码，唯一，非空
	 */
	private String phone;
	/**
	 * 密码，非空
	 */
	private String password;
	/**
	 * 年龄
	 */
	private Integer age;
	/**
	 * 性别【0-男，1-女】
	 */
	private Integer sex;
	/**
	 * 头像图片
	 */
	private String avatarImage;
	/**
	 * 角色【0-普通用户，1-管理员，2-超级管理员】
	 */
	private Integer role;
	/**
	 * 添加时间
	 */
	private LocalDateTime addTime;
	/**
	 * 更新时间
	 */
	private LocalDateTime updateTime;

}
