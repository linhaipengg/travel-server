package com.peng.travel.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.peng.common.utils.PageUtils;
import com.peng.travel.user.entity.UserEntity;

import java.util.Map;

/**
 * 
 *
 * @author peng
 * @email haipeng_lin@163.com
 * @date 2024-03-24 09:25:21
 */
public interface UserService extends IService<UserEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

