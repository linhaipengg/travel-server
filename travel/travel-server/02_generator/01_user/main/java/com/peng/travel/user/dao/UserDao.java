package com.peng.travel.user.dao;

import com.peng.travel.user.entity.UserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author peng
 * @email haipeng_lin@163.com
 * @date 2024-03-24 09:25:21
 */
@Mapper
public interface UserDao extends BaseMapper<UserEntity> {
	
}
