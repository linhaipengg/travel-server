package com.peng.travel.statistics.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.peng.common.utils.PageUtils;
import com.peng.common.utils.Query;

import com.peng.travel.statistics.dao.SensitiveWordDao;
import com.peng.travel.statistics.entity.SensitiveWordEntity;
import com.peng.travel.statistics.service.SensitiveWordService;


@Service("sensitiveWordService")
public class SensitiveWordServiceImpl extends ServiceImpl<SensitiveWordDao, SensitiveWordEntity> implements SensitiveWordService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SensitiveWordEntity> page = this.page(
                new Query<SensitiveWordEntity>().getPage(params),
                new QueryWrapper<SensitiveWordEntity>()
        );

        return new PageUtils(page);
    }

}