package com.peng.travel.statistics.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author peng
 * @email haipeng_lin@163.com
 * @date 2024-03-24 10:10:56
 */
@Data
@TableName("sms_like")
public class LikeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 点赞id，自增
	 */
	@TableId
	private Integer likeId;
	/**
	 * 用户id
	 */
	private String userId;
	/**
	 * 景点id列表
	 */
	private String attractionId;
	/**
	 * 添加时间
	 */
	private LocalDateTime addTime;
	/**
	 * 修改时间
	 */
	private LocalDateTime updateTime;

}
