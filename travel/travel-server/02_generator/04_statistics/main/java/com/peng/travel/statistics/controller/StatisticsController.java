package com.peng.travel.statistics.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.peng.travel.statistics.entity.StatisticsEntity;
import com.peng.travel.statistics.service.StatisticsService;
import com.peng.common.utils.PageUtils;
import com.peng.common.utils.R;



/**
 * 
 *
 * @author peng
 * @email haipeng_lin@163.com
 * @date 2024-03-24 10:10:56
 */
@Slf4j
@RestController
@RequestMapping("statistics/statistics")
public class StatisticsController {
    @Autowired
    private StatisticsService statisticsService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissionss("statistics:statistics:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = statisticsService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{statisticsId}")
    //@RequiresPermissionss("statistics:statistics:info")
    public R info(@PathVariable("statisticsId") Integer statisticsId){
		StatisticsEntity statistics = statisticsService.getById(statisticsId);

        return R.ok().put("statistics", statistics);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissionss("statistics:statistics:save")
    public R save(@RequestBody StatisticsEntity statistics){
		statisticsService.save(statistics);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissionss("statistics:statistics:update")
    public R update(@RequestBody StatisticsEntity statistics){
		statisticsService.updateById(statistics);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissionss("statistics:statistics:delete")
    public R delete(@RequestBody Integer[] statisticsIds){
		statisticsService.removeByIds(Arrays.asList(statisticsIds));

        return R.ok();
    }

}
