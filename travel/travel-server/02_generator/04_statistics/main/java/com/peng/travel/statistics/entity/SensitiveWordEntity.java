package com.peng.travel.statistics.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author peng
 * @email haipeng_lin@163.com
 * @date 2024-03-24 10:10:56
 */
@Data
@TableName("sms_sensitive_word")
public class SensitiveWordEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 敏感词id
	 */
	@TableId
	private Integer sensitiveWordId;
	/**
	 * 敏感词
	 */
	private String sensitiveWord;
	/**
	 * 添加时间
	 */
	private LocalDateTime addTime;
	/**
	 * 修改时间
	 */
	private LocalDateTime updateTime;

}
