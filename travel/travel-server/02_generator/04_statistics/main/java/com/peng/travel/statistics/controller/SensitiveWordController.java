package com.peng.travel.statistics.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.peng.travel.statistics.entity.SensitiveWordEntity;
import com.peng.travel.statistics.service.SensitiveWordService;
import com.peng.common.utils.PageUtils;
import com.peng.common.utils.R;



/**
 * 
 *
 * @author peng
 * @email haipeng_lin@163.com
 * @date 2024-03-24 10:10:56
 */
@Slf4j
@RestController
@RequestMapping("statistics/sensitiveword")
public class SensitiveWordController {
    @Autowired
    private SensitiveWordService sensitiveWordService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissionss("statistics:sensitiveword:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = sensitiveWordService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{sensitiveWordId}")
    //@RequiresPermissionss("statistics:sensitiveword:info")
    public R info(@PathVariable("sensitiveWordId") Integer sensitiveWordId){
		SensitiveWordEntity sensitiveWord = sensitiveWordService.getById(sensitiveWordId);

        return R.ok().put("sensitiveWord", sensitiveWord);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissionss("statistics:sensitiveword:save")
    public R save(@RequestBody SensitiveWordEntity sensitiveWord){
		sensitiveWordService.save(sensitiveWord);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissionss("statistics:sensitiveword:update")
    public R update(@RequestBody SensitiveWordEntity sensitiveWord){
		sensitiveWordService.updateById(sensitiveWord);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissionss("statistics:sensitiveword:delete")
    public R delete(@RequestBody Integer[] sensitiveWordIds){
		sensitiveWordService.removeByIds(Arrays.asList(sensitiveWordIds));

        return R.ok();
    }

}
