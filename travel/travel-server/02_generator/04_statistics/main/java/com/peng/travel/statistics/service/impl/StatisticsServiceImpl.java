package com.peng.travel.statistics.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.peng.common.utils.PageUtils;
import com.peng.common.utils.Query;

import com.peng.travel.statistics.dao.StatisticsDao;
import com.peng.travel.statistics.entity.StatisticsEntity;
import com.peng.travel.statistics.service.StatisticsService;


@Service("statisticsService")
public class StatisticsServiceImpl extends ServiceImpl<StatisticsDao, StatisticsEntity> implements StatisticsService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<StatisticsEntity> page = this.page(
                new Query<StatisticsEntity>().getPage(params),
                new QueryWrapper<StatisticsEntity>()
        );

        return new PageUtils(page);
    }

}