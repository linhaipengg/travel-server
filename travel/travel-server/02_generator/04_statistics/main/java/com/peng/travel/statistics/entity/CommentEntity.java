package com.peng.travel.statistics.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author peng
 * @email haipeng_lin@163.com
 * @date 2024-03-24 10:10:56
 */
@Data
@TableName("sms_comment")
public class CommentEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 评论id，自增
	 */
	@TableId
	private Integer commentId;
	/**
	 * 用户id
	 */
	private Integer userId;
	/**
	 * 景点id
	 */
	private Integer attractionId;
	/**
	 * 评论内容
	 */
	private String comment;
	/**
	 * 添加时间
	 */
	private LocalDateTime addTime;
	/**
	 * 修改时间
	 */
	private LocalDateTime updateTime;

}
