package com.peng.travel.statistics.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.peng.common.utils.PageUtils;
import com.peng.travel.statistics.entity.LikeEntity;

import java.util.Map;

/**
 * 
 *
 * @author peng
 * @email haipeng_lin@163.com
 * @date 2024-03-24 10:10:56
 */
public interface LikeService extends IService<LikeEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

