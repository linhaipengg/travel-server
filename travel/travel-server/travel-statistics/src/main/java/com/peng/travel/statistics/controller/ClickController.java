package com.peng.travel.statistics.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.peng.travel.statistics.entity.ClickEntity;
import com.peng.travel.statistics.service.ClickService;
import com.peng.common.utils.PageUtils;
import com.peng.common.utils.R;



/**
 * 
 *
 * @author peng
 * @email haipeng_lin@163.com
 * @date 2024-03-24 10:10:56
 */
@Slf4j
@RestController
@RequestMapping("statistics/click")
public class ClickController {
    @Autowired
    private ClickService clickService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissionss("statistics:click:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = clickService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{clickId}")
    //@RequiresPermissionss("statistics:click:info")
    public R info(@PathVariable("clickId") Integer clickId){
		ClickEntity click = clickService.getById(clickId);

        return R.ok().put("click", click);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissionss("statistics:click:save")
    public R save(@RequestBody ClickEntity click){
		clickService.save(click);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissionss("statistics:click:update")
    public R update(@RequestBody ClickEntity click){
		clickService.updateById(click);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissionss("statistics:click:delete")
    public R delete(@RequestBody Integer[] clickIds){
		clickService.removeByIds(Arrays.asList(clickIds));

        return R.ok();
    }

}
