package com.peng.travel.statistics.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.peng.travel.statistics.entity.CollectionEntity;
import com.peng.travel.statistics.service.CollectionService;
import com.peng.common.utils.PageUtils;
import com.peng.common.utils.R;



/**
 * 
 *
 * @author peng
 * @email haipeng_lin@163.com
 * @date 2024-03-24 10:10:56
 */
@Slf4j
@RestController
@RequestMapping("statistics/collection")
public class CollectionController {
    @Autowired
    private CollectionService collectionService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissionss("statistics:collection:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = collectionService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{collectionId}")
    //@RequiresPermissionss("statistics:collection:info")
    public R info(@PathVariable("collectionId") Integer collectionId){
		CollectionEntity collection = collectionService.getById(collectionId);

        return R.ok().put("collection", collection);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissionss("statistics:collection:save")
    public R save(@RequestBody CollectionEntity collection){
		collectionService.save(collection);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissionss("statistics:collection:update")
    public R update(@RequestBody CollectionEntity collection){
		collectionService.updateById(collection);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissionss("statistics:collection:delete")
    public R delete(@RequestBody Integer[] collectionIds){
		collectionService.removeByIds(Arrays.asList(collectionIds));

        return R.ok();
    }

}
