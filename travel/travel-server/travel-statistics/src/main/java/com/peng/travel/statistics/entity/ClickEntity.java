package com.peng.travel.statistics.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 
 * 
 * @author peng
 * @email haipeng_lin@163.com
 * @date 2024-03-24 10:10:56
 */
@Data
@TableName("sms_click")
public class ClickEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 点击id，自增
	 */
	@TableId(type= IdType.AUTO)
	private Integer clickId;
	/**
	 * 用户id
	 */
	private String userId;
	/**
	 * 景点id
	 */
	private Integer attractionId;
	/**
	 * 点击数
	 */
	private Integer clicks;
	/**
	 * 添加时间
	 */
	@TableField(fill = FieldFill.INSERT)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Shanghai")
	private LocalDateTime addTime;
	/**
	 * 修改时间
	 */
	@TableField(fill = FieldFill.UPDATE)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Shanghai")
	private LocalDateTime updateTime;

}
