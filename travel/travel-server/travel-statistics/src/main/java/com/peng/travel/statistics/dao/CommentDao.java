package com.peng.travel.statistics.dao;

import com.peng.travel.statistics.entity.CommentEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author peng
 * @email haipeng_lin@163.com
 * @date 2024-03-24 10:10:56
 */
@Mapper
public interface CommentDao extends BaseMapper<CommentEntity> {
	
}
