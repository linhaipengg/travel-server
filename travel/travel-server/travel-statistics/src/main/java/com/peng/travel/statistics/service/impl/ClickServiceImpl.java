package com.peng.travel.statistics.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.peng.common.utils.PageUtils;
import com.peng.common.utils.Query;

import com.peng.travel.statistics.dao.ClickDao;
import com.peng.travel.statistics.entity.ClickEntity;
import com.peng.travel.statistics.service.ClickService;


@Service("clickService")
public class ClickServiceImpl extends ServiceImpl<ClickDao, ClickEntity> implements ClickService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ClickEntity> page = this.page(
                new Query<ClickEntity>().getPage(params),
                new QueryWrapper<ClickEntity>()
        );

        return new PageUtils(page);
    }

}