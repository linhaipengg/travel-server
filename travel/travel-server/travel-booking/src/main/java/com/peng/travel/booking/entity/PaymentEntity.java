package com.peng.travel.booking.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 
 * 
 * @author peng
 * @email haipeng_lin@163.com
 * @date 2024-03-24 10:01:48
 */
@Data
@TableName("bms_payment")
public class PaymentEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 支付记录id，全局id
	 */
	@TableId(value="payment_id",type= IdType.AUTO)
	private Integer paymentId;
	/**
	 * 支付金额
	 */
	private Double price;
	/**
	 * 支付状态【1为成功，0为失败】
	 */
	private Integer status;
	/**
	 * 添加时间
	 */
	@TableField(fill = FieldFill.INSERT)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Shanghai")
	private LocalDateTime addTime;
	/**
	 * 修改时间
	 */
	@TableField(fill = FieldFill.UPDATE)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Shanghai")
	private LocalDateTime updateTime;

}
