package com.peng.travel.booking.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.peng.travel.booking.entity.TicketEntity;
import com.peng.travel.booking.service.TicketService;
import com.peng.common.utils.PageUtils;
import com.peng.common.utils.R;



/**
 * 
 *
 * @author peng
 * @email haipeng_lin@163.com
 * @date 2024-03-24 10:01:48
 */
@Slf4j
@RestController
@RequestMapping("booking/ticket")
public class TicketController {
    @Autowired
    private TicketService ticketService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissionss("booking:ticket:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = ticketService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{ticketId}")
    //@RequiresPermissionss("booking:ticket:info")
    public R info(@PathVariable("ticketId") Integer ticketId){
		TicketEntity ticket = ticketService.getById(ticketId);

        return R.ok().put("ticket", ticket);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissionss("booking:ticket:save")
    public R save(@RequestBody TicketEntity ticket){
		ticketService.save(ticket);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissionss("booking:ticket:update")
    public R update(@RequestBody TicketEntity ticket){
		ticketService.updateById(ticket);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissionss("booking:ticket:delete")
    public R delete(@RequestBody Integer[] ticketIds){
		ticketService.removeByIds(Arrays.asList(ticketIds));

        return R.ok();
    }

}
