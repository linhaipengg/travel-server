package com.peng.travel.booking.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 
 * 
 * @author peng
 * @email haipeng_lin@163.com
 * @date 2024-03-24 10:01:48
 */
@Data
@TableName("bms_ticket_type")
public class TicketTypeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 门票类型id，自增
	 */
	@TableId(value="ticket_type_id",type= IdType.AUTO)
	private Integer ticketTypeId;
	/**
	 * 门票类型【成人票、学生票、儿童票、老人票、团体票】
	 */
	private String type;
	/**
	 * 门票类型介绍
	 */
	private String introduction;
	/**
	 * 添加时间
	 */
	@TableField(fill = FieldFill.INSERT)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Shanghai")
	private LocalDateTime addTime;
	/**
	 * 修改时间
	 */
	@TableField(fill = FieldFill.UPDATE)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Shanghai")
	private LocalDateTime updateTime;

}
