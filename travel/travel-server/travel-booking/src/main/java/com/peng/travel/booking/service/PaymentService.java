package com.peng.travel.booking.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.peng.common.utils.PageUtils;
import com.peng.travel.booking.entity.PaymentEntity;

import java.util.Map;

/**
 * 
 *
 * @author peng
 * @email haipeng_lin@163.com
 * @date 2024-03-24 10:01:48
 */
public interface PaymentService extends IService<PaymentEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

