package com.peng.travel.booking.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.peng.common.utils.PageUtils;
import com.peng.common.utils.Query;

import com.peng.travel.booking.dao.TicketTypeDao;
import com.peng.travel.booking.entity.TicketTypeEntity;
import com.peng.travel.booking.service.TicketTypeService;


@Service("ticketTypeService")
public class TicketTypeServiceImpl extends ServiceImpl<TicketTypeDao, TicketTypeEntity> implements TicketTypeService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<TicketTypeEntity> page = this.page(
                new Query<TicketTypeEntity>().getPage(params),
                new QueryWrapper<TicketTypeEntity>()
        );

        return new PageUtils(page);
    }

}