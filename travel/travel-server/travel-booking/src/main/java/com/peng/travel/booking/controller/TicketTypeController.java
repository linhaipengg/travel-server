package com.peng.travel.booking.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.peng.travel.booking.entity.TicketTypeEntity;
import com.peng.travel.booking.service.TicketTypeService;
import com.peng.common.utils.PageUtils;
import com.peng.common.utils.R;



/**
 * 
 *
 * @author peng
 * @email haipeng_lin@163.com
 * @date 2024-03-24 10:01:48
 */
@Slf4j
@RestController
@RequestMapping("booking/tickettype")
public class TicketTypeController {
    @Autowired
    private TicketTypeService ticketTypeService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissionss("booking:tickettype:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = ticketTypeService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{ticketTypeId}")
    //@RequiresPermissionss("booking:tickettype:info")
    public R info(@PathVariable("ticketTypeId") Integer ticketTypeId){
		TicketTypeEntity ticketType = ticketTypeService.getById(ticketTypeId);

        return R.ok().put("ticketType", ticketType);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissionss("booking:tickettype:save")
    public R save(@RequestBody TicketTypeEntity ticketType){
		ticketTypeService.save(ticketType);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissionss("booking:tickettype:update")
    public R update(@RequestBody TicketTypeEntity ticketType){
		ticketTypeService.updateById(ticketType);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissionss("booking:tickettype:delete")
    public R delete(@RequestBody Integer[] ticketTypeIds){
		ticketTypeService.removeByIds(Arrays.asList(ticketTypeIds));

        return R.ok();
    }

}
