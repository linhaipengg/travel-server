package com.peng.travel.booking.dao;

import com.peng.travel.booking.entity.TicketEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author peng
 * @email haipeng_lin@163.com
 * @date 2024-03-24 10:01:48
 */
@Mapper
public interface TicketDao extends BaseMapper<TicketEntity> {
	
}
