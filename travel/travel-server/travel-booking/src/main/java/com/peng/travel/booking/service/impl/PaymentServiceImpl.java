package com.peng.travel.booking.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.peng.common.utils.PageUtils;
import com.peng.common.utils.Query;

import com.peng.travel.booking.dao.PaymentDao;
import com.peng.travel.booking.entity.PaymentEntity;
import com.peng.travel.booking.service.PaymentService;


@Service("paymentService")
public class PaymentServiceImpl extends ServiceImpl<PaymentDao, PaymentEntity> implements PaymentService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<PaymentEntity> page = this.page(
                new Query<PaymentEntity>().getPage(params),
                new QueryWrapper<PaymentEntity>()
        );

        return new PageUtils(page);
    }

}