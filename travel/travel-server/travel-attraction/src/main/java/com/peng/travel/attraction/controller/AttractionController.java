package com.peng.travel.attraction.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.peng.travel.attraction.entity.AttractionEntity;
import com.peng.travel.attraction.service.AttractionService;
import com.peng.common.utils.PageUtils;
import com.peng.common.utils.R;



/**
 * 
 *
 * @author peng
 * @email haipeng_lin@163.com
 * @date 2024-03-24 09:46:23
 */
@Slf4j
@RestController
@RequestMapping("attraction/attraction")
public class AttractionController {
    @Autowired
    private AttractionService attractionService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissionss("attraction:attraction:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = attractionService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{attractionId}")
    //@RequiresPermissionss("attraction:attraction:info")
    public R info(@PathVariable("attractionId") Integer attractionId){
		AttractionEntity attraction = attractionService.getById(attractionId);

        return R.ok().put("attraction", attraction);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissionss("attraction:attraction:save")
    public R save(@RequestBody AttractionEntity attraction){
		attractionService.save(attraction);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissionss("attraction:attraction:update")
    public R update(@RequestBody AttractionEntity attraction){
		attractionService.updateById(attraction);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissionss("attraction:attraction:delete")
    public R delete(@RequestBody Integer[] attractionIds){
		attractionService.removeByIds(Arrays.asList(attractionIds));

        return R.ok();
    }

}
