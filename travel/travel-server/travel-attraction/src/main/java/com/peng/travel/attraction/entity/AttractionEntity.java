package com.peng.travel.attraction.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 
 * 
 * @author peng
 * @email haipeng_lin@163.com
 * @date 2024-03-24 09:46:23
 */
@Data
@TableName("ams_attraction")
public class AttractionEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 景点id，自增
	 */
	@TableId(value="attraction_id",type= IdType.AUTO)
	private Integer attractionId;
	/**
	 * 景点名称
	 */
	private String attractionName;
	/**
	 * 景点资源类型id
	 */
	private Integer resourceTypeId;
	/**
	 * 景点等级
	 */
	private Integer qualityGrade;
	/**
	 * 景点所在省份城市
	 */
	private String provinceCity;
	/**
	 * 详细位置
	 */
	private String location;
	/**
	 * 开放时间
	 */
	private String openHour;
	/**
	 * 电话
	 */
	private String phone;
	/**
	 * 景点介绍
	 */
	private String introduction;
	/**
	 * 景点图片列表
	 */
	private String images;
	/**
	 * 状态【1为显示，0为不显示】
	 */
	private Integer status;
	/**
	 * 添加时间
	 */
	@TableField(fill = FieldFill.INSERT)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Shanghai")
	private LocalDateTime addTime;
	/**
	 * 修改时间
	 */
	@TableField(fill = FieldFill.UPDATE)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Shanghai")
	private LocalDateTime updateTime;

}
