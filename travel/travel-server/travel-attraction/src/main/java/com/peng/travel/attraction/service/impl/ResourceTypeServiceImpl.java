package com.peng.travel.attraction.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.peng.common.utils.PageUtils;
import com.peng.common.utils.Query;

import com.peng.travel.attraction.dao.ResourceTypeDao;
import com.peng.travel.attraction.entity.ResourceTypeEntity;
import com.peng.travel.attraction.service.ResourceTypeService;


@Service("resourceTypeService")
public class ResourceTypeServiceImpl extends ServiceImpl<ResourceTypeDao, ResourceTypeEntity> implements ResourceTypeService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ResourceTypeEntity> page = this.page(
                new Query<ResourceTypeEntity>().getPage(params),
                new QueryWrapper<ResourceTypeEntity>()
        );

        return new PageUtils(page);
    }

}