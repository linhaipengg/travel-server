package com.peng.travel.attraction.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 
 * 
 * @author peng
 * @email haipeng_lin@163.com
 * @date 2024-03-24 09:46:23
 */
@Data
@TableName("ams_resource_type")
public class ResourceTypeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 资源类型id，自增
	 */
	@TableId(value="resource_type_id",type= IdType.AUTO)
	private Integer resourceTypeId;
	/**
	 * 资源类型【自然类、人文类、复合类、主题公园、社会类】
	 */
	private String resourceType;
	/**
	 * 资源类型介绍
	 */
	private String introduction;
	/**
	 * 添加时间
	 */
	@TableField(fill = FieldFill.INSERT)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Shanghai")
	private LocalDateTime addTime;
	/**
	 * 修改时间
	 */
	@TableField(fill = FieldFill.UPDATE)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Shanghai")
	private LocalDateTime updateTime;

}
